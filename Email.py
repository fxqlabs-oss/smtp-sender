from __future__ import annotations
import smtplib
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from pathlib import Path
from typing import List

class SmtpEmail(object):
    def __init__(self):
        self._host = None   # type: str
        self._port = None  # type: int
        self._username = None   # type: str
        self._password = None   # type: str
        self._toRecipients = None  # type: List[str]
        self._ccRecipients = None   # type: List[str]
        self._from = None  # type: str
        self._subject = None  # type: str
        self._body = None  # type: str
        self._attachments = None  # type: List[str]
        self._use_login = False # type: bool
        self._use_tls = False # type: bool
        self._use_ehlo = False # type: bool
        self._built = False # type: bool

    def send(self) -> None:
        if self._built is False: raise Exception("Message must be constructed through the builder, please construct via the SmtpBuilder")
        smtp_server = smtplib.SMTP(self._host, self._port)
        if self._use_ehlo is True: smtp_server.ehlo()
        if self._use_tls is True: smtp_server.starttls()
        if self._use_login is True: smtp_server.login(self._username, self._password)

        msg = MIMEMultipart()
        msg['Subject'] = self._subject if self._subject is not None else "NO SUBJECT"
        msg['To'] = ",".join(self._toRecipients)
        if self._ccRecipients is not None: msg['CC'] = ",".join(self._ccRecipients)
        msg['From'] = self._from

        msg.attach(MIMEText(self._body, 'plain'))

        for attachment_path in self._attachments:
            path = Path(attachment_path)
            attachment = open(path.as_posix(), "rb")
            part = MIMEBase('application', 'octet-stream')
            part.set_payload((attachment).read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', "attachment; filename= %s" % path.name)
            msg.attach(part)

        smtp_server.sendmail(
            from_addr=self._from,
            to_addrs=self._toRecipients,
            msg=msg.as_string()
        )

        smtp_server.quit()

    class SmtpEmailBuilder(object):
        _host = None   # type: str
        _port = None  # type: int
        _username = None   # type: str
        _password = None   # type: str
        _toRecipients = None  # type: List[str]
        _ccRecipients = None   # type: List[str]
        _from = None  # type: str
        _subject = None  # type: str
        _body = None  # type: str
        _attachments = None  # type: List[str]
        _use_tls = False # type: bool
        _use_ehlo = False # type: bool

        @classmethod
        def toHost(cls, host: str) -> SmtpEmailBuilder:
            cls._host = host
            return cls

        @classmethod
        def toPort(cls, port: int) -> SmtpEmailBuilder:
            cls._port = port
            return cls

        @classmethod
        def withUsername(cls, username: str) -> SmtpEmailBuilder:
            cls._username = username
            return cls

        @classmethod
        def withPassword(cls, password: str) -> SmtpEmailBuilder:
            cls._password = password
            return cls

        @classmethod
        def toRecipients(cls, recipients: List[str]) -> SmtpEmailBuilder:
            cls._toRecipients = recipients
            return cls

        @classmethod
        def toCcRecipients(cls, ccRecipients: [str]) -> SmtpEmailBuilder:
            cls._ccRecipients = ccRecipients
            return cls

        @classmethod
        def fromAddress(cls, fromAddress: str) -> SmtpEmailBuilder:
            cls._from = fromAddress
            return cls

        @classmethod
        def withSubject(cls, subject: str) -> SmtpEmailBuilder:
            cls._subject = subject
            return cls

        @classmethod
        def withBody(cls, body: str) -> SmtpEmailBuilder:
            cls._body = body
            return cls

        @classmethod
        def withAttachments(cls, attachments: List[str]) -> SmtpEmailBuilder:
            cls._attachments = attachments
            return cls

        @classmethod
        def usingEhlo(cls) -> SmtpEmailBuilder:
            cls._use_ehlo = True
            return cls

        @classmethod
        def usingTls(cls) -> SmtpEmailBuilder:
            cls._use_tls = True
            return cls

        @classmethod
        def build(cls) -> SmtpEmail:
            if cls._host is None: raise Exception("Unable to send mail without host")
            if cls._port is None: raise Exception("Unable to send mail without port")
            if cls._toRecipients is None: raise Exception("Unable to send the mail without any recipients")
            if cls._body is None: raise Exception("Unable to send a blank email, please define a body")
            if cls._from is None: raise Exception("Unable to send an email without a from address")

            smtpEmail = SmtpEmail()
            smtpEmail._host = cls._host
            smtpEmail._port = cls._port
            smtpEmail._toRecipients = cls._toRecipients
            smtpEmail._ccRecipients = cls._ccRecipients
            smtpEmail._from = cls._from
            smtpEmail._subject = cls._subject
            smtpEmail._body = cls._body

            if(cls._attachments is not None):
                smtpEmail._attachments = cls._attachments

            if(cls._username and cls._password is not None):
                smtpEmail._use_login = True
                smtpEmail._username = cls._username
                smtpEmail._password = cls._password

            smtpEmail._use_tls = cls._use_tls
            smtpEmail._use_ehlo = cls._use_ehlo

            smtpEmail._built = True

            return smtpEmail