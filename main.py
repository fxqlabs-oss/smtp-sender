from Email import SmtpEmail

SmtpEmail.SmtpEmailBuilder\
    .toHost("smtp.fxquants.net")\
    .toPort(25)\
    .fromAddress("noreply@fxqlabs.net")\
    .toRecipients(["Jonathan.Turnock@fxqlabs.net"])\
    .withSubject("Test Subject for the email")\
    .withBody("Test Email")\
    .withAttachments(["/Users/jonathan.turnock/PycharmProjects/mail-sender/ExampleFile.txt"])\
    .build().send()